#include "player.h"
#define MAX_DEPTH 12
/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
 
int weights[8][8];
 
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
    this->board = new Board();
    this->myside = side;
    
    
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            if (i == 0 || i == 7) {
                if (j == 0 || j == 7) {
                    weights[i][j] = CORNER_WEIGHT;
                } else if (j == 1 || j == 6) {
                    weights[i][j] = ADJCORNER_WEIGHT;
                } else {
                    weights[i][j] = EDGE_WEIGHT;
                }
            } else if (i == 1 || i == 6) {
                if (j == 0 || j == 1 || j == 6 || j == 7) {
                    weights[i][j] = ADJCORNER_WEIGHT;
                } else {
                    weights[i][j] = ADJEDGE_WEIGHT;
                }
            } else {
                if (j == 0 || j == 7) {
                    weights[i][j] = EDGE_WEIGHT;
                } else if (j == 1 || j == 6) {
                    weights[i][j] = ADJEDGE_WEIGHT;
                } else {
                    weights[i][j] = 0;
                }
            }
        }
    }
    // for (int i = 0; i < 8; i++) {
        // for (int j = 0; j < 8; j++) {
            // std::cerr << weights[i][j] << " ";
            // if (j == 7) {
            // std::cerr << std::endl;
            // }
        // }
    // }
     
}

/*
 * Destructor for the player.
 */
Player::~Player() {
    delete board;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
     
    /* Hello Andrew */
    /* Hi max */
    Board * board = this->board;
    Side other = (this->myside == BLACK) ? WHITE : BLACK;
    board->doMove(opponentsMove, other); 
    Move * m = new Move(0,0);
    Board * copy;
    if (board->hasMoves(this->myside)) {
        MoveScore maxms(1,1,-1000000);
        for (unsigned int i = 0; i < (board->availablemoves).size(); i++) {
            copy = board->copy();
            calc_min_score(copy, this->myside, &(board->availablemoves[i]), 1);
            board->availablemoves[i].score +=
            weights[board->availablemoves[i].y][board->availablemoves[i].x];
            if ((board->availablemoves[i]).score >= maxms.score) {
                maxms = board->availablemoves[i];
            }
            delete copy;
        } 
        m->setX(maxms.x);
        m->setY(maxms.y);
        board->doMove(m, this->myside);
        return m;
    }
    
    return NULL;
}

int calc_score(Board * board, Side side, Move move, bool self) {
    Side other = (side == BLACK) ? WHITE : BLACK;
    if (self) {
        //return board->count(side) - board->count(other);
        return weights[move.getY()][move.getX()] + board->count(side) - board->count(other);
    } else {
        //return board->count(side) - board->count(other);
        return -weights[move.getY()][move.getX()] + board->count(side) - board->count(other);
    }
}

void calc_min_score(Board * board, Side side, MoveScore * movescore, int depth) {
    Move move(movescore->x, movescore->y);
    Side other = (side == BLACK) ? WHITE : BLACK;
    board->doMove(&move, side);
    if (depth >= MAX_DEPTH) {
        if (depth % 2 == 0) {
            movescore->score = calc_score(board, other, move, false);
        } else {
            movescore->score = calc_score(board, side, move, true);
        }
    } else {
        if (board->hasMoves(other)) {
            int minscore = 1000000;
            Board * copy;
            for (unsigned int i = 0; i < (board->availablemoves).size(); i++) {
                copy = board->copy();
                move.setX(board->availablemoves[i].x);
                move.setY(board->availablemoves[i].y);
                depth++;
                calc_min_score(copy, other, &(board->availablemoves[i]), depth); 
                if (minscore > (board->availablemoves[i]).score) {
                    minscore = (board->availablemoves[i]).score;
                }
                delete copy;
            }
            movescore->score = minscore;
        }
    }
}