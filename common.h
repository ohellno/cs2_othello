#ifndef __COMMON_H__
#define __COMMON_H__
#include <vector>
#include <iostream>

#define CORNER_WEIGHT       25
#define ADJCORNER_WEIGHT    -10
#define EDGE_WEIGHT         5
#define ADJEDGE_WEIGHT      10

enum Side { 
    WHITE, BLACK
};

class Move {
   
public:
    int x, y;
    Move(int x, int y) {
        this->x = x;
        this->y = y;        
    }
    ~Move() {}

    int getX() { return x; }
    int getY() { return y; }

    void setX(int x) { this->x = x; }
    void setY(int y) { this->y = y; }
};
extern int weights[8][8];
#endif
