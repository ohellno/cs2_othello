We made our AI by designing and writing the code together. First, we
implemented a pseudo-random player that would take the first
available move that it found. Then, we decided to make a heuristic
by making an array of weights for each position on the board. Just by
adding the heuristic, we were able to beat SimplePlayer and sometimes
ConstantTimePlayer. We then designed and wrote the pseudocode for a 
minimax algorithm. Afterwards, we coded and debugged it. Our first
minimax attempt worked but had a few bugs. After many iterations, we
cleaned up the code and adjusted weights/depth such that we could
consistently beat SimplePlayer, ConstantTimePlayer, and BetterPlayer.

Our AI implements a minimax algorithm along with a score heuristic that
depends on the board position weighting as well as the difference in 
pieces between our side and the opponents. The board weighting
weights corners extremely heavily. Positions adjacent to corners are
weighted very negatively to prevent corners from being taken. Edges
that are not adjacent to corners are weighted positively as well.
In addition, we added a positive weight to positions that are
adjacent to edges to implement our Bait-and-Switch algorithm in the
hopes that other players will take the edges but leave us the corners
such that we can take over the edges of the board. Our minimax algorithm
goes to a depth of twelve and maximizes our minimum score according to
the stone count and weights.
