#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"

using namespace std;

class Player {
    Board * board;
    Side myside;
    
public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};

int calc_score(Board * board, Side side, Move move, bool self);
void calc_min_score(Board * board, Side side, MoveScore * movescore, int depth);
#endif
